# DevOps / CICD example project

## Running modes
### Production mode
```console
$ PORT=8080 npm run start
```
### Build project
```console
$ npm run build
```
### Development project
```console
$ PORT=8080 npm run dev
```
